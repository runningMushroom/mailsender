# Functionality
* CreateHtmlMessage:
    * Pass paramaters to html template & send it as an email.  

# Target framework
* netcoreapp3.1

# Instructions
* Add a new directory to your wwwroot folder to house your html templates
* Create the templates you would like to use in your email bodies
* Add MailServerSettings to appsettings.json
    * "MailServerSettings": {"From": "from name", "Password": "serverpassword", "SmtpServer": "smtpDomain", "Username": "smtpUsername", "TemplateFolder": "dirname"}
* Add services.ConfigureMailSender(Configuration) to your Startup.cs file