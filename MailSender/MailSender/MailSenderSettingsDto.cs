﻿namespace MailSender
{
    public class MailSenderSettingsDto
    {
        public string From { get; set; }
        public string Password { get; set; }
        public string SmtpServer { get; set; }
        public string Username { get; set; }
        public int Port { get; set; }

        // this is the folder under wwwroot where mail templates will be kept
        public string TemplateFolder { get; set; }
    }
}