﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace MailSender
{
    public static class MailSenderExtension
    {
        public static void ConfigureMailSender(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(config.GetSection("MailServerSettings").Get<MailSenderSettingsDto>());
            services.AddSingleton<IMailSender, MailSenderRepo>();
        }
    }
}