﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailSender
{
    public class MailSenderMessageDto
    {
        public string Subject { get; set; }

        // string name, string email
        public Dictionary<string, string> CcUsers { get; set; }

        public Dictionary<string, string> ToUsers { get; set; }
    }
}