﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MailSender
{
    public class MailSenderRepo : IMailSender
    {
        private readonly IHostingEnvironment _host;
        private readonly MailSenderSettingsDto _settings;
        private readonly ILogger _logger;

        public MailSenderRepo(IHostingEnvironment host, MailSenderSettingsDto settings, ILogger logger)
        {
            _host = host;
            _settings = settings;
            _logger = logger;
        }

        public void CreateHtmlMessage(MailSenderMessageDto message, string templateName,
            params object[] htmlBodyParams)
        {
            try
            {
                var mm = new MimeMessage();
                mm.From.Add(new MailboxAddress(_settings.From, _settings.Username));
                mm.Subject = message.Subject;

                // cc users if there are any
                if (message.CcUsers.Count > 0)
                    foreach (KeyValuePair<string, string> kvp in message.CcUsers)
                    {
                        mm.Cc.Add(new MailboxAddress(kvp.Key, kvp.Value));
                    }

                // add users to send to
                foreach (KeyValuePair<string, string> kvp in message.ToUsers)
                {
                    mm.Cc.Add(new MailboxAddress(kvp.Key, kvp.Value));
                }

                // set path to templates in wwwroot
                var template = Path.Join(_host.WebRootPath, _settings.TemplateFolder, templateName);

                // Initialize body builder
                BodyBuilder bb = new BodyBuilder();

                // Read the html template file
                using (StreamReader sr = File.OpenText(template))
                {
                    bb.HtmlBody = sr.ReadToEnd();
                }

                // insert html message paramaters
                bb.HtmlBody = string.Format(bb.HtmlBody, htmlBodyParams);

                // set message body
                mm.Body = bb.ToMessageBody();

                SendEmail(mm);
            }
            catch (Exception e)
            {
                _logger.LogError($"MailSender.CreateHtmlMessage Error: {e.Message}");
                throw;
            }
        }

        private void SendEmail(MimeMessage message)
        {
            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect(_settings.SmtpServer, _settings.Port, SecureSocketOptions.None);
                    client.Authenticate(_settings.Username, _settings.Password);
                    client.Send(message);
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"MailSender.SendEmail Error: {e.Message}");
                throw;
            }
        }
    }
}