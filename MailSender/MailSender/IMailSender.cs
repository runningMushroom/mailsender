﻿using MimeKit;
using System;

namespace MailSender
{
    public interface IMailSender
    {
        void CreateHtmlMessage(MailSenderMessageDto message, string templateName,
            params object[] htmlBodyParams);
    }
}